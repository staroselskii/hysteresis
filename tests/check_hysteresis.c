#include <stdlib.h>
#include <check.h>
#include <hysteresis.h>

START_TEST(test_level_1)
{
    unsigned int level = 0;
    hysteresis_reset();

    level = hysteresis(9);
    ck_assert_int_eq(level, 0);
    level = hysteresis(15);
    ck_assert_int_eq(level, 1);
    level = hysteresis(11);
    ck_assert_int_eq(level, 1);
    level = hysteresis(10);
    ck_assert_int_eq(level, 0);
}
END_TEST

START_TEST(test_level_2)
{
    unsigned int level = 0;
    hysteresis_reset();

    level = hysteresis(41);
    ck_assert_int_eq(level, 2);

    level = hysteresis(37);
    ck_assert_int_eq(level, 2);

    level = hysteresis(36);
    ck_assert_int_eq(level, 2);

    level = hysteresis(35);
    ck_assert_int_eq(level, 1);

}

START_TEST(test_bounds_assymetry)
{
    unsigned int level = 0;
    hysteresis_reset();

    level = hysteresis(38);
    ck_assert_int_eq(level, 2);

    level = hysteresis(64);
    ck_assert_int_eq(level, 2);

    level = hysteresis(65);
    ck_assert_int_eq(level, 3);
}

START_TEST(test_undefined_initial_level_in_deadzone)
{
    unsigned int level = 0;

    hysteresis_reset();
    level = hysteresis(11);
    ck_assert_int_eq(level, 0);

    hysteresis_reset();
    level = hysteresis(13);
    ck_assert_int_eq(level, 0);

    hysteresis_reset();
    level = hysteresis(38);
    ck_assert_int_eq(level, 2);

    hysteresis_reset();
    level = hysteresis(36);
    ck_assert_int_eq(level, 1);

    hysteresis_reset();
    level = hysteresis(63);
    ck_assert_int_eq(level, 3);

    hysteresis_reset();
    level = hysteresis(62);
    ck_assert_int_eq(level, 2);

    hysteresis_reset();
    level = hysteresis(88);
    ck_assert_int_eq(level, 4);

    hysteresis_reset();
    level = hysteresis(87);
    ck_assert_int_eq(level, 3);

    hysteresis_reset();
    level = hysteresis(200);
    ck_assert_int_eq(level, 4);
}
END_TEST


Suite* hysteresis_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("Hysteresis");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_level_1);
    tcase_add_test(tc_core, test_level_2);
    tcase_add_test(tc_core, test_undefined_initial_level_in_deadzone);
    tcase_add_test(tc_core, test_bounds_assymetry);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void)
{
    int number_failed;
    Suite *suite;
    SRunner *runner;

    suite = hysteresis_suite();
    runner = srunner_create(suite);

    srunner_run_all(runner, CK_VERBOSE);
    number_failed = srunner_ntests_failed(runner);
    srunner_free(runner);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
