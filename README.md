# Overview

This is a project that shows how to work with the processes that have hysteresis.


# Build

```mkdir build && cd build && cmake .. && make```

# Testing

Testing is performed with using a header-only [Check](https://github.com/libcheck/check) library.

You can run the test binary manually after building by launching a check_hysteresis binary.

 
# Running

```./hysteresys```

Then just enter the value you want.

The output for 1 is "For 1 level is 0".

Ctrl-D to exit.

There's absolutely no error handling in this entrypoint. So beware.
