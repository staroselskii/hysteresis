#include <stdio.h>
#include <hysteresis.h>

int main() {
    unsigned int level = 0;
    int input = 0;
    int ret = 0;

    do {
        ret = scanf("%d", &input);

        level = hysteresis(input);
        printf("For %d level is %d!\n", input, level);
    } while (ret != EOF);

    return 0;
}
