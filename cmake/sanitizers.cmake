#
# This file contains a set of functions for enabling the following sanitizers:
#   - AddressSanitizer (LeakSanitizer built-in)
#   - ThreadSanitizer
#   - UndefinedBehaviorSanitizer
#
# All required compiler flags are set to CMAKE_SAN_C_CXX_FLAGS and are exposed to the parent scope.
# This variable doesn't do anything, to actually set these flags one needs to append them to CMAKE_C[XX]_FLAGS.
#
# TODO: rework it from global level flags to target level ones, i.e. use target_* API
#

function(_enable_common_sanitizer_flags)
    if(CMAKE_SAN_C_CXX_FLAGS)
        return()
    endif()

    macro(_force_remove_c_cxx_flag FLAG_REGEX)
        STRING(REGEX REPLACE "${FLAG_REGEX}" "" CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG}")
        STRING(REGEX REPLACE "${FLAG_REGEX}" "" CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG}")
        set(CMAKE_CXX_FLAGS_DEBUG ${CMAKE_CXX_FLAGS_DEBUG} CACHE STRING "" FORCE)
        set(CMAKE_C_FLAGS_DEBUG ${CMAKE_C_FLAGS_DEBUG} CACHE STRING "" FORCE)
    endmacro()

    if(MSVC)
        list(APPEND C_CXX_FLAGS "/Zi")
        # /RTC must be removed as it's not compatible with ASAN: https://docs.microsoft.com/en-us/cpp/sanitizers/asan
        # Unfortunately, there is no way to do it in CMake other than to manually remove it from CMAKE_C[XX]_FLAGS_DEBUG
        # as it's added automatically on configure. Release builds doesn't enforce it.
        _force_remove_c_cxx_flag("/RTC[1csu]*")
    else()
        list(APPEND C_CXX_FLAGS "-g")
        list(APPEND C_CXX_FLAGS "-O1")
        list(APPEND C_CXX_FLAGS "-fno-omit-frame-pointer")
    endif()

    list(JOIN C_CXX_FLAGS " " C_CXX_FLAGS)
    set(CMAKE_SAN_C_CXX_FLAGS "${C_CXX_FLAGS}" PARENT_SCOPE)
endfunction()

function(_assert_mutual_exclusiveness)
    # From https://gcc.gnu.org/onlinedocs/gcc/Instrumentation-Options.html:
    # > -fsanitize=address
    # >     [...] The option cannot be combined with -fsanitize=thread [...]
    if (STUDIO_ENABLE_ASAN AND STUDIO_ENABLE_TSAN)
        message(FATAL_ERROR "AddressSanitizer and ThreadSanitizer cannot be used simultaneously.")
    endif()
endfunction()

function(enable_address_sanitizer)
    _assert_mutual_exclusiveness()
    _enable_common_sanitizer_flags()
    if(MSVC)
        set(CMAKE_SAN_C_CXX_FLAGS "${CMAKE_SAN_C_CXX_FLAGS} /fsanitize=address" PARENT_SCOPE)
    else()
        set(CMAKE_SAN_C_CXX_FLAGS "${CMAKE_SAN_C_CXX_FLAGS} -fsanitize=address" PARENT_SCOPE)
    endif()
endfunction()

function(enable_thread_sanitizer)
    if(NOT UNIX)
        message(WARNING "ThreadSanitizer is not supported on this platform yet.")
        return()
    endif()

    _assert_mutual_exclusiveness()
    _enable_common_sanitizer_flags()
    set(CMAKE_SAN_C_CXX_FLAGS "${CMAKE_SAN_C_CXX_FLAGS} -fsanitize=thread" PARENT_SCOPE)
endfunction()

function(enable_undefined_behavior_sanitizer)
    if(NOT UNIX)
        message(WARNING "UndefinedBehaviorSanitizer is not supported on this platform yet.")
        return()
    endif()

    _enable_common_sanitizer_flags()
    set(CMAKE_SAN_C_CXX_FLAGS "${CMAKE_SAN_C_CXX_FLAGS} -fsanitize=undefined" PARENT_SCOPE)
endfunction()
