#include <stdbool.h>
#include "hysteresis.h"

enum DiscreteLevel {
    LEVEL_0 = 0,
    LEVEL_1 = 1,
    LEVEL_2 = 2,
    LEVEL_3 = 3,
    LEVEL_4 = 4,
    NUM_OF_LEVELS,
};

static const unsigned int HYSTERESIS_THRESHOLD = 2;
static const unsigned int cutoffs[NUM_OF_LEVELS + 1] = {0, 12, 37, 62, 87, 100};

static enum DiscreteLevel look_up_level(unsigned int input_percent) {
    unsigned int level;
    for (level = 0; level < NUM_OF_LEVELS; level++) {
        if (input_percent >= cutoffs[level] && input_percent <= cutoffs[level + 1])
            break;
    }
    return (enum DiscreteLevel) level;
}

static enum DiscreteLevel state = LEVEL_0;

unsigned int hysteresis(unsigned int input_percent)
{
    /* If the value is bigger than the highest defined level just return the largest level  */

    if (input_percent >= cutoffs[NUM_OF_LEVELS]) {
        state = LEVEL_4;
        return state;
    }

    enum DiscreteLevel current_level = state;

    unsigned int lower_bound = cutoffs[current_level];
    if (current_level > LEVEL_0) {
        lower_bound -= HYSTERESIS_THRESHOLD;
    }

    unsigned int upper_bound = cutoffs[current_level + 1];
    if (current_level < LEVEL_4) {
        /* We need to take into account that the deadband is asymmetric.
         * We need to roll over after the middle of the given range.
         * Each deadband has 5 values. The first two are lower than the
         * threshold whereas the second tree are higher.
         * Take for example [60, 65] with the threshold of 62.5. If the
         * previous value was on the Level 2 we should roll over on 65
         * */
        upper_bound = upper_bound + HYSTERESIS_THRESHOLD + 1;
    }

    bool is_value_on_the_same_level = (input_percent > lower_bound && input_percent < upper_bound);
    if (is_value_on_the_same_level) {
        return current_level;
    }

    state = look_up_level(input_percent);
    return state;
}
void hysteresis_reset(void)
{
    state = LEVEL_0;
}
